## Workflow

## Folder structure
┌───/
│   ├.gitignore                                    * Files to be excluded from the git repo
│   ├package.json                                  * NPM package file
│   ├tsconfig.json                                 * TypeScript configuration file
│   ├typings.json                                  * Typings for type hinting
│   │
│   ├───src/
│   │   ├index.ts                                  * Imports and exports
│   │   │
│   │   ├───pipes/
│   │   │   ├index.ts                              * Imports and exports of pipes
│   │   │   │
│   │   │   └───...
│   │   │
│   │   ├───services/
│   │   │   ├index.ts                              * Imports and exports of services
│   │   │   │
│   │   │   ├───api/                               * General API services
│   │   │   │   ├cache-service.ts                  * Base caching service
│   │   │   │   ├error-service.ts                  * Base error handling service
│   │   │   │   ├pagination-service.ts             * Base pagination/cursor service
│   │   │   │   ├request-service.ts                * Base HTTP requests service
│   │   │   │   └routing-service.ts                * Base routing service
│   │   │   │
│   │   │   ├───app/                               * General app services
│   │   │   │   └helpers-service.ts                * Base helpers
│   │   │   │
│   │   │   ├───authentication/                    * General authentication services
│   │   │   │   └authentication-service.ts         * Authentication (OAuth, JWT) service
│   │   │   │
│   │   │   ├───config/
│   │   │   │   └config-service.ts                 * Base configuration service
│   │   │   │
│   │   │   ├───storage/
│   │   │   │   └storage-service.ts                * Storage service for data (Local, Session, Cookie)
│   │   │   │
│   │   │   └───user/
│   │   │       └user-service.ts                   * User service (authorized?, base user info)
│   │   │
│   │   ├───validators/
│   │   │   ├index.ts                              * Imports and exports of validators
│   │   │   │
│   │   │   └───...