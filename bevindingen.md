## Bevindingen
  * Git workflow is (bijna) niet aanwezig (of was wel aanwezig maar niet consistent toegepast)
  * Code workflow, huidige code in angular-shared-apm en angular-shared-generic zijn niet consistent of niet duidelijk (whitelines, benamingen, indentatie, code uitgecomment in de repository)
  * Het handmatig instellen van een endpoint werkt niet goed
  * Om een component te kunnen implementeren moet je in zowel de dashboard repository als de angular-shared-apm repository code wijzigen (ieder een branch, te ver uitgetrokken, taken verdelen over shared libraries en dashboard is ongewenst; werkt developers tegen met workflow)
  * Benaming van bestanden geven in eerste instantie niet aan wat hun taak is (template, interface, component, service?)
  * Er wordt een JWT package geimport in de shared libraries, maar ipv dat wordt er een eigen implementatie gebruikt, wat is de reden hiervoor?

## Propositie oplossing
  * Modulaire filosofie (dashboard)
  * Auth service (authorisatie; angular-shared-generic)
  * URI service (routing/data handling; angular-shared-apm)

## Voorbeeld folder structuur
   │───src/
   │   │   app.ts                                 * Base app, routing, rendering (App / Error / Login / ...)
   │   │   main.ts                                * Base app setup (imports, services etc)
   │   │   polyfills.ts                           * Polyfills for functionality that isn't available in all targeted browsers/devices
   │   │
   │   ├───app/
   │   |   |
   │   |   ├───services/                          * Place for all our services (OAuth2, URI)
   │   │   │      auth.svc.ts                     * Auth service, general service around the authorization process (OAuth2 / JWT / ...)
   │   │   │      routing.svc.ts                  * Modular routing where we can set method, url, data (Dashboard, AuPair, Family, Conversations...)
   │   │   │      conversations.svc.ts            * Conversations service (CRUD for conversations/messages, use base service?)
   │   │   │
   │   │   ├───core/                              * Core application, encapsulate services etc.
   │   │   │
   │   │   ├───error/                             * Error page, where we handle errors (showing error page to user, logging errors for developers)
   │   │   │
   │   │   ├───login/                             * Login page, where we handle user login (authorize on correct credentials)
   │   │   │
   │   │   └───components/                        * Place for all our components (Home, AuPair, Family, Conversations...)
   │   │       │
   │   │       ├───conversations/
   │   │       │      conversations.comp.ts       * Conversations (and messages) component
   │   │       │      conversation.itf.ts         * Conversation interface
   │   │       │      conversation.style.scss     * Conversations styling
   │   │       │      conversations.tpl.html      * Conversations view template
   │   │       │      message.itf.ts              * Message interface
   │   │       │      messages.style.scss         * Messages styling
   │   │       │      messages.tpl.html           * Conversation messages view template
   │   │       │
   │   │       ├───…
   │   │       │
   │   │       …
   │   │
   │   ├───assets/                                * Application assets
   │   │
   │   │───config/                                * Application configurations
   │   │
   │   │───scss/                                  * Application styling
   │   │